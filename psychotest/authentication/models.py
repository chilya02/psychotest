from cgi import test
from email.policy import default
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .managers import CustomUserManager
from django.template.defaultfilters import slugify

ROLES = [
    ('DC', 'Специалист'),
    ('PC', 'Пациент'),
]

class CustomUser(AbstractUser):

    username = None

    is_autorised = models.BooleanField(default=False)
    role = models.CharField(max_length=2, choices=ROLES, default='PC', verbose_name='Тип аккаунта')

    first_name = models.CharField(max_length=15, verbose_name='Имя')
    middle_name = models.CharField(max_length=15, verbose_name='Отчество')
    last_name = models.CharField(max_length=15, verbose_name='Фамилия')

    birthday=models.DateField(auto_now=False, verbose_name='ДР',)
    email = models.EmailField(_('email'), unique=True)
    slug = models.SlugField(verbose_name='slug')
    avatar = models.ImageField(upload_to='users', blank=True, null=True, verbose_name='Фото', default='/static/img/login.svg')

    specialist = models.ForeignKey(
        'self', 
        on_delete=models.SET_NULL, 
        null=True, 
        blank=True, 
        verbose_name='Специалист', 
        related_name='pacients'
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [
        'first_name', 
        'middle_name', 
        'last_name', 
        'birthday', 
    ] 

    objects = CustomUserManager()



    def get_oficial_name(self):
        return f'{self.last_name} {self.first_name[0]}. {self.middle_name[0]}.' 

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        self.slug = slugify(self.email)
        super(CustomUser, self).save(*args, **kwargs)
