from email import message
from urllib.request import HTTPRedirectHandler
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
import json
from authentication.models import CustomUser

from .forms import CustomUserCreationForm, LogInForm

'''
class RegisterView(CreateView):
    form_class = CustomUserCreationForm
    template_name = 'authentication/register.html'
    success_url = reverse_lazy("role")

    def form_valid(self, form):
        #save the new user first
        form.save()
        #get the username and password
        email = form.cleaned_data['email']
        password = form.cleaned_data['password1']
        #authenticate user then login
        user = authenticate(email=email, password=password)
        login(self.request, user)
        return super(RegisterView, self).form_valid(form)
'''

def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return HttpResponse(json.dumps({'status': 'success'}))
        else:
            errors = json.loads(form.errors.as_json())
            message = {
                'errors': errors,
                'status': 0
                }
            return HttpResponse(json.dumps(message))
    else:
        form = CustomUserCreationForm
        return render(
            request, 
            'authentication/register.html',
            context={
                'form': form,
                'authenticated': False,
                'auth_process': True
                })


def select_user_role(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            user = CustomUser.objects.get(email=request.user.email)
            if user.is_autorised:
                return redirect('/')
            context={
                     'authenticated': True,
                     'user': user,
                 }
            return render(
                request=request, 
                template_name="authentication/select_role.html",
                context=context,
            )
                 
        if request.method == 'POST':
            user = CustomUser.objects.get(email=request.user.email)
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            user.role = body['role']
            print(user)
            print(body['role'])
            user.save()
            return HttpResponse(json.dumps({'status': 'success'}))
        else:
            return HttpResponse(json.dumps({'status': '0'}))
    else:
        return redirect('login')


def auth(request):
    if request.method == 'POST':
        form = LogInForm(request.POST)
        if form.is_valid():
            user=authenticate(
                username=form.cleaned_data['email'],
                password=form.cleaned_data['password'],
            )
            if user is not None:
                login(request, user)
                return HttpResponse(json.dumps({'status': 'success'}))
            else:
                return HttpResponse(json.dumps({'status': '0'}))
    else:
        if request.user.is_authenticated:
            return redirect('/lk/')
        else:
            form = LogInForm()
            return render(
                request=request, 
                template_name="authentication/login.html",
                context={
                    'form': form,
                    'authenticated': False,
                    'auth_process': True
            })


def select_spec(request):
    secret_code = 'doctor_shtopor2001'
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if request.method == 'POST':
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            code = body['code']
            print(user)
            print(body['code'])
            if code == secret_code:
                user.is_autorised = True
                user.save()
                return HttpResponse(json.dumps({'status': 'success'}))
            else:
                return HttpResponse(json.dumps({'status': 'fail'}))
        else:
            context = {
                'authenticated': True,
                'user': user
            }
            return render(
                request=request, 
                template_name="authentication/select_specialist.html", 
                context=context
                )
    return redirect('login')
    

def select_pacient(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            user = CustomUser.objects.get(email=request.user.email)
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            specialist_email = body['specislist']
            user.specialist = CustomUser.objects.get(email=specialist_email)
            user.is_autorised = True
            user.save()
            return HttpResponse(json.dumps({'status': 'success'}))
        else:
            specialists = CustomUser.objects.filter(is_autorised=True).filter(role='DC')
            user = CustomUser.objects.get(email=request.user.email)
            context={
                    'specialists': specialists,
                    'authenticated': True,
                    'user': user,
                    }
            return render(
                request=request, 
                template_name="authentication/select_pacient.html", 
                context=context)

    else:
        return redirect('login')


def change_data(request):
    pass