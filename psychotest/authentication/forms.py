from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm

from .models import CustomUser, ROLES

class CustomUserCreationForm(UserCreationForm):

    password1 = forms.CharField(
        label='Пароль', 
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-input'
                }
            )
        )
    password2 = forms.CharField(
        label='Повторите пароль',
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-input'
                }
            )
        )
    first_name = forms.CharField(
        label='Имя', 
        widget=forms.TextInput(
            attrs={
                'class': 'form-input'
                }
            )
        )
    middle_name = forms.CharField(
        label='Отчество', 
        widget=forms.TextInput(
            attrs={
                'class': 'form-input'
                }
            )
        )
    last_name = forms.CharField(
        label='Фамилия', 
        widget=forms.TextInput(
            attrs={
                'class': 'form-input'
                }
            )
        )
    birthday = forms.DateField(
        label='Дата рождения',
        input_formats=['%d.%m.%Y'], widget=forms.DateInput(
            attrs={
                'class': 'form-input',
                'placeholder': 'дд.мм.гггг',
                'onkeyup': '''  let v = this.value;
                                if (v.match(/^\d{2}$/) !== null) {
                                    this.value = v + '.';
                                } else if (v.match(/^\d{2}\.\d{2}$/) !== null) {
                                    this.value = v + '.';
                                }'''
                }
            )
        )
    email = forms.EmailField(
        label='email', 
        widget=forms.EmailInput(
            attrs={
                'class': 'form-input'
                }
            )
        )

    class Meta:

        model = CustomUser
        fields = ("last_name", "first_name", "middle_name", "birthday","email")

class CustomUserChangeForm(UserChangeForm):
    email = forms.EmailField(
    label='email', 
    widget=forms.EmailInput(
        attrs={
            'class': 'form-input'
            }
        )
    )

    class Meta:
        model = CustomUser
        fields = ("email",)

class CustomUserPasswordChangeForm(PasswordChangeForm):
        error_css_class = 'has-error'
        error_messages = {'password_incorrect':
                  "Неправильный пароль"}
        old_password = forms.CharField(required=True, label='Συνθηματικό',
                      widget=forms.PasswordInput(attrs={
                        'class': 'form-control'}),
                      error_messages={
                        'required': 'Το συνθηματικό δε μπορεί να είναι κενό'})

        new_password1 = forms.CharField(required=True, label='Συνθηματικό',
                      widget=forms.PasswordInput(attrs={
                        'class': 'form-control'}),
                      error_messages={
                        'required': 'Το συνθηματικό δε μπορεί να είναι κενό'})
        new_password2 = forms.CharField(required=True, label='Συνθηματικό (Επαναλάβατε)',
                      widget=forms.PasswordInput(attrs={
                        'class': 'form-control'}),
                      error_messages={
                        'required': 'Το συνθηματικό δε μπορεί να είναι κενό'})

class LogInForm(forms.Form):
    
    email = forms.EmailField(label='email', widget=forms.EmailInput(attrs={'class': 'form-input'}))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-input'}))