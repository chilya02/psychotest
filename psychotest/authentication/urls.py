from django.urls import path
from django.contrib.auth.views import LogoutView
from . import views

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.auth, name='login'),
    path('role/', views.select_user_role, name='role'),
    path('selectspec/', views.select_spec, name='select_spec'),
    path('selectpacient/', views.select_pacient, name='select_pacient'),
    path("logout/", LogoutView.as_view(), name="logout"),
    path('change', views.change_data),
]