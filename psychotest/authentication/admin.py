from unicodedata import name
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# Register your models here.

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser

class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ["email", "role", "is_autorised"]

    ordering = ('email',)

    fieldsets = (
        (
            None,{
                'fields': ('email', 'avatar', 'birthday', 'last_name', 'first_name', 'middle_name', 'role', 'specialist', 'slug')
                }
            ),
            (
                'Permissions', {
                    'fields': ('is_staff', 'is_active', 'is_autorised')
                    }
                ),
        )
    add_fieldsets = (
        (
            None, {
                'fields': ('email', 'password1', 'password2', 'birthday', 'last_name', 'first_name', 'middle_name', 'role', 'specialist', 'slug')
                }
            ),
            (
            'Permissions', {
                'fields': ('is_staff', 'is_active', 'is_autorised')
                }
            ),
        )

    

admin.site.register(CustomUser, CustomUserAdmin)