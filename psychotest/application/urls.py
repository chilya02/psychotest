from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('lk/', views.lk, name='lk'), 
    path('about/', views.about),
    path('questions/', views.questions),
    path('contacts/', views.contacts),
    path('pacient/new', views.pacient_new_tests),
    path('pacient/recent', views.pacient_recent_tests),
    path('pacient/recent/<int:id>', views.test_result),
    path('pacient/tests/try/<int:id>', views.pacient_test_annotation),
    path('pacient/tests/start/<int:id>', views.pacient_test),
    path('specialist/tests', views.specialist_tests),
    path('specialist/tests/<int:id>', views.specialist_test_view), 
    path('specialist/tests/add', views.specialist_new_test),
    path('specialist/test_results/<int:id>', views.test_result),
    path('specialist/pacients', views.specialist_pacients),
    path('specialist/pacients/<str:slug>', views.pacient_card),
]