from django.shortcuts import redirect, render
from django.http import HttpResponse
from authentication.models import CustomUser
from .models import Diagnosis, Test, Question, Answer, Passed_test, Given_answer
from authentication.forms import CustomUserChangeForm, CustomUserPasswordChangeForm
import json

def index(request):
    context = {}
    if request.user.is_authenticated:
        context['authenticated'] = True
        user = CustomUser.objects.get(email=request.user.email)
        context['user'] = user
    else:
        context['authenticated'] = False
    return render(request=request, template_name='application/index.html', context=context)


def lk(request):

    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised:
            if request.method == 'POST' and request.FILES:
                user.avatar=request.FILES['myfile1']
                user.save()
                return redirect('lk')
            context = {
                'authenticated': True,
                'user': user,
                'form': CustomUserChangeForm,
                'password_form': CustomUserPasswordChangeForm,

                }
            return render(request=request, template_name='application/lk.html', context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def pacient_new_tests(request):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised:
            new_tests = user.test_set.all()
            test_quantity = len(new_tests)
            tests_data = []
            for test in new_tests:
                tests_data.append({})
                tests_data[-1]['name'] = test.name
                tests_data[-1]['quantity'] = test.questions_quantity()
                tests_data[-1]['id'] = test.id
            context = {
                'authenticated': True,
                'user': user,
                'new_tests': tests_data,
                'tests_quantity': test_quantity
                }
            return render(request=request, template_name='application/pacient_new_tests.html', context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def test_result(request, id):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        test = Passed_test.objects.get(id=id)

        if user.is_autorised:
            context = {
                'authenticated': True,
                'user': user,
                'test': test,
                }

            return render(request=request, template_name='application/test_result.html', context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def pacient_recent_tests(request):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        tests = user.passed_tests.all()
        if user.is_autorised:
            context = {
                'authenticated': True,
                'user': user,
                'tests': tests,
                }
            return render(request=request, template_name='application/pacient_recent.html', context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def pacient_card(request, slug):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'DC'):
            pacient = CustomUser.objects.get(slug=slug)

            if request.method == 'POST':
                body_unicode = request.body.decode('utf-8')
                body = json.loads(body_unicode)
                test = Test.objects.get(id=body['test'])
                test.users.add(pacient)
                return HttpResponse(json.dumps({'status': 'success'}))
            all_tests = Test.objects.all()
            new_tests = pacient.test_set.all()
            tests = Passed_test.objects.filter(user=pacient)
            test_quantity = len(new_tests)
            tests_data = []
            for test in new_tests:
                tests_data.append({})
                tests_data[-1]['name'] = test.name
                tests_data[-1]['quantity'] = test.questions_quantity()
                tests_data[-1]['id'] = test.id
            context = {
                'authenticated': True,
                'user': user,
                'pacient': pacient,
                'new_tests': tests_data,
                'tests': tests,
                'all_tests': all_tests,
                'tests_quantity': test_quantity
                }
            return render(request=request, template_name='application/pacient_card.html', context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def specialist_pacients(request):

    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'DC'):
            pacients_request = user.pacients.all()
            pacients = []
            for pacient in pacients_request:
                pacients.append({})
                pacients[-1]['name'] = pacient.get_oficial_name()
                pacients[-1]['birthday'] = pacient.birthday.strftime('%d.%m.%Y')
                pacients[-1]['slug'] = pacient.slug
            context = {
                'authenticated': True,
                'user': user,
                'pacients': pacients
                }
            return render(
                request=request,
                template_name='application/specialist_pacients.html',
                context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def specialist_tests(request):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'DC'):
            tests = Test.objects.all()
            tests_data = []
            for test in tests:
                tests_data.append({})
                tests_data[-1]['name'] = test.name
                tests_data[-1]['quantity'] = test.questions_quantity()
                tests_data[-1]['id'] = test.id
            context = {
                'authenticated': True,
                'user': user,
                'tests_data': tests_data,
                'tests_quantity': len(tests_data)
                }
            return render(
                request=request,
                template_name='application/specialist_tests.html',
                context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def my_parse(data):
    questions_count = data['questions']
    test = Test.objects.create(name=data['name'], annotation=data['annotation'])
    test.save()
    for i in range(3):
        diagnosis = Diagnosis.objects.create(
            name=data[f'd-{i+1}-name'],
            max_mark=data[f'd-{i+1}-max'],
            recommendations=data[f'd-{i+1}-rec'],
            test=test,
            )
        diagnosis.save()
    for index in range(questions_count):
        question = Question.objects.create(
            text=data[f'q-{index+1}'],
            test=test,
            )
        question.save()
        for ans_index in range(data[f'{index + 1}']):
            answer = Answer.objects.create(
                text=data[f'q-{index+1}-a-{ans_index+1}'],
                mark=data[f'q-{index+1}-a-m-{ans_index+1}'],
                question=question,
                )
            answer.save()


def specialist_new_test(request):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'DC'):
            if request.method == 'POST':
                body_unicode = request.body.decode('utf-8')
                body = json.loads(body_unicode)
                my_parse(body)
                return HttpResponse(json.dumps({'status': 'success'}))
            context = {
                'authenticated': True,
                'user': user,
                }
            return render(
                request=request,
                template_name='application/specialist_new_test.html',
                context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def pacient_test_annotation(request, id):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'PC'):
            test = Test.objects.get(id=id)
            context = {
                'authenticated': True,
                'user': user,
                'test': test,
                }
            return render(
                request=request,
                template_name='application/pacient_test_annotation.html',
                context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')


def pacient_test(request, id):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'PC'):
            test = Test.objects.get(id=id)
            if request.method == 'POST':
                body_unicode = request.body.decode('utf-8')
                body = json.loads(body_unicode)
                new_test = Passed_test.objects.create(test=test, user=user)
                new_test.save()
                user.test_set.remove(test)
                for key in body:
                    answer = Given_answer.objects.create(
                        test=new_test,
                        question=Question.objects.get(id=key),
                        answer=Answer.objects.get(id=body[key])
                        )
                    answer.save()

                return HttpResponse(json.dumps({'status': 'success', 'id': new_test.id}))
                

            context = {
                'authenticated': True,
                'user': user,
                'test': test,
                }
                
            return render(
                request=request,
                template_name='application/pacient_test_start.html',
                context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')



    pass


def specialist_test_view(request, id):
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        if user.is_autorised & (user.role == 'DC'):
            test = Test.objects.get(id=id)
            context = {
                'authenticated': True,
                'user': user,
                'test': test,
                }
            return render(
                request=request,
                template_name='application/specialist_test_view.html',
                context=context)
        else:
            return redirect('/authentication/role/')
    else:
        return redirect('/authentication/login/')

def about(request):
    context = {}
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        context = {
            'authenticated': True,
            'user': user,
            }
    else:
        context = {
            'authenticated': False,
        }
    return render(
        request=request,
        template_name='application/about.html',
        context=context)


def questions(request):
    context = {}
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        context = {
            'authenticated': True,
            'user': user,
            }
    else:
        context = {
            'authenticated': False,
        }
    return render(
        request=request,
        template_name='application/questions.html',
        context=context)

def contacts(request):
    context = {}
    if request.user.is_authenticated:
        user = CustomUser.objects.get(email=request.user.email)
        context = {
            'authenticated': True,
            'user': user,
            }
    else:
        context = {
            'authenticated': False,
        }
    return render(
        request=request,
        template_name='application/contacts.html',
        context=context)
