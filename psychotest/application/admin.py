from django.contrib import admin
from .models import Test, Diagnosis, Question, Answer, Passed_test, Given_answer

admin.site.register((Test, Diagnosis, Question, Answer, Passed_test, Given_answer))