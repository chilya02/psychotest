#from __future__ import annotations, unicode_literals
from statistics import mode
from django.db import models
from authentication.models import CustomUser
from django.template.defaultfilters import slugify

# Create your models here.

class Test(models.Model):
    '''
        diagnosis - Возвращает список диагнозов
        questions - Список вопросов
    '''
    name = models.CharField(max_length=100, verbose_name='Название теста', unique=False)
    annotation = models.CharField(max_length=500, verbose_name='Аннотация', unique=False)

    users = models.ManyToManyField(CustomUser)
    
    def __str__(self):
        return self.name

    def questions_quantity(self):
        return self.questions.all().count



class Diagnosis(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название диагноза', unique=False)
    max_mark = models.PositiveIntegerField(verbose_name='Верхняя граница диапазона', unique=False)
    recommendations = models.CharField(max_length=500, verbose_name='Рекоммендации', unique=False)
    test = models.ForeignKey(Test, verbose_name='Тест', on_delete=models.CASCADE, related_name='diagnosis')

    def __str__(self):
        return self.name


class Question(models.Model):
    text = models.CharField(max_length=300, verbose_name='Текст вопроса', unique=False)
    test = models.ForeignKey(Test, on_delete=models.CASCADE, verbose_name='Тест', related_name='questions')
    
    def __str__(self):
        return self.text


class Answer(models.Model):
    text = models.CharField(max_length=100, verbose_name='Текст ответа', unique=False)
    mark = models.PositiveIntegerField(verbose_name='Балл', unique=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name='Вопрос', related_name='answers')

    def __str__(self):
        return self.text


class Passed_test(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, verbose_name='Пользователь', related_name='passed_tests', unique=False)
    test = models.ForeignKey(Test, on_delete=models.CASCADE, verbose_name='Тест', unique=False)
    date = models.DateField(auto_now=True, verbose_name='Дата теста', unique=False)

    def mark(self) -> int:
        sum = 0
        for answer in self.answers.all():
            sum += answer.answer.mark
        return sum

    def diagnosis(self):
        mark = self.mark()
        all_diagnosis = self.test.diagnosis.all().order_by('-max_mark')
        user_diagnosis = None
        for diagnosis in all_diagnosis:
            if mark <= diagnosis.max_mark:
                user_diagnosis = diagnosis
        return user_diagnosis


class Given_answer(models.Model):
    test = models.ForeignKey(Passed_test, on_delete=models.CASCADE, verbose_name='Тест', related_name='answers', unique=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, verbose_name='Вопрос', unique=False)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, verbose_name='Ответ', unique=False)
